clear all
close all
clc
aa = 0 ;   %[m]
bb = 0.1 ; %[m]

n = 51 ;
x = linspace(aa,bb, n) ;
dx = x(2) - x(1) ;
h = dx ;
wbLt = 'D' ;
wbRt = 'D' ;

wbLw =  0 ;
wbRw =  0 ;
f_fh = @(x) ones(size(x)) ;
% inicjalizacja zmiennych potrzebnych w trakcie obliczen
A = zeros(n,n) ;
b = zeros(n,1) ;

% PROCESSING
% assembling 
tic 
for ii = 2:n-1
  A(ii,ii) = -2/dx^2 ;
  A(ii,ii-1) = 1/dx^2 ;
  A(ii,ii+1) = 1/dx^2 ;
end
toc
b = f_fh( x(:) ) ;
% PROCESSING
% uwzglednienie warunkow brzegowych
if wbRt == 'D'
  A(n,n) = 1 ;
  b(n,1) = wbRw ;
elseif wbRt == 'N'
  A(n,n)    = -2/dx^2 ;
  A(n, n-1) =  2/dx^2 ; 
  b(n,1) = -2*h*wbRw/dx^2 ;
else 
  error('Nieznany typ war. brzeg.')   
end

% z lewej strony
if wbLt == 'D'
  A(1,1) = 1 ;
  b(1,1) = wbLw ;
 elseif wbLt == 'N'
  A(1,1) = -2/dx^2 ;
  A(1,2) =  2/dx^2 ; 
  b(1,1) =  -2*h*wbLw/dx^2 ;
 else 
  error('Nieznany typ war. brzeg.')   
end 

T = A \ b ;
% 3. POST processing 
plot(x,T, 'ro')
hold on
T_exact = @(x) x.^2 - 0.05 * x;
plot(x, T_exact(x), 'b-')
hold off
