close all 
clear all 
clc 

%% DEFINICJA PARAMETROW ZAGADNIENIA
% warunek poczatkowy
t0 = 1 ;
y0 = 3 ;

% rownanie rozniczkowe zw. 1 rz
f = @(t, y) -y.^2 + t*0; 


% 
tk = 5.0 ;
dt = 0.1 ;
t = t0:dt:tk ;
y = zeros( size(t)) ;
n = length(t) ;
y(1) = y0 ;

% metoda Eulera
for ii = 1:n-1
  fi = f(t(ii), y(ii) ) ;
  dt = t(ii+1) - t(ii) ;
  y(ii+1) = y(ii) + fi * dt ;
end 

plot(t,y) 
hold on 
plot(t, 1./(t+1), 'r' ) 
hold off 

