clear all 
close all 
clc 

% 1. PRE processing 
% deklaracja/inicjalizacja zmiennych
aa = 0 ;
bb = 4 ; 

cc = 0 ;
dd = 2 ;

h = 0.1 ;

xx = aa:h:bb ;
yy = cc:h:dd ;

nx = length(xx) ;  % liczba wezlow w kier. x
ny = length(yy) ;  % liczba wezlow w kier. y
n = nx*ny ; % liczba wszystkich wezlow

[X,Y] = meshgrid(xx,yy) ;
plot(X,Y, 'ro') 

NODES = reshape (1:n, ny,nx) ;

TAB = [NODES(:), X(:), Y(:)]

% implementacja rownania Poissona
% rownanie - niejednorodne
% prawa strona != 0


% inicjalizacja zmiennych potrzebnych w trakcie obliczen
A = zeros(n,n) ;
b = zeros(n,1) ;

% 2. Processing
% assembling 

for kk = 1:n
  
  A(kk,kk) = -4/h^2 ;

  if (kk - 1 >= 1)
  A(kk,kk-1) = 1/h^2 ;
  end 

  if (kk + 1 <= n )
  A(kk,kk+1) = 1/h^2 ;
  end 

  if (kk - ny >= 1)
  A(kk,kk-ny) = 1/h^2 ;
  end 

  if (kk +ny <= n)
  A(kk,kk+ny) = 1/h^2 ;
  end 
  
end

%% WARUNKI BRZEGOWE 

% Zał. 1 - warunek brzegowy jest tego samego typu na kazdym boku 

% Typ warunku brzegowego 
BCT = { 'D', % dol 
        'D', % prawy
        'D', % gora
        'D'};% lewa  

% wezly brzegowe 
BNODES = { NODES(1, 2:end-1), % dol 
           NODES(:,end), % prawa 
           NODES(end, 2:end-1), % gora
           NODES(:,1)}; % lewa 
        
% Wartosci warunkow brzegowych 
BCF = { @(x,y) 0 * ones(size(x)) ;
        @(x,y) 1 * ones(size(x)) ;
        @(x,y) 0 * ones(size(x)) ;
        @(x,y) 0 * ones(size(x)) ;
       } ;

BCV = { BCF{1}(X(BNODES{1}(:)) , Y(BNODES{1}(:)) );
        BCF{2}(X(BNODES{2}(:)) , Y(BNODES{2}(:)) );
        BCF{3}(X(BNODES{3}(:)) , Y(BNODES{3}(:)) );
        BCF{4}(X(BNODES{4}(:)) , Y(BNODES{4}(:)) );
       } ;
        
BIG  = 10^8 ;
% uwzglednienie warunkow brzegowych:
for pp=1:4
  WEZLY_NA_ptym_boku = BNODES{pp}(:) ;
  liczba_wezlow_na_ptym_boku = length(WEZLY_NA_ptym_boku) ;
  
  for qq = 1:liczba_wezlow_na_ptym_boku
    kk = WEZLY_NA_ptym_boku(qq) ;
    if BCT{pp} == 'D'
      akk = A(kk,kk) ;
      A(kk,kk) = akk * BIG ;
      b(kk,1) = akk * BIG * BCV{pp}(qq) ;
    end
    
    if BCT{pp} == 'N'
      error('TODO')
    end
  
  end 
end 


   
T = A \ b ;

TT = reshape(T,ny,nx) ;
% 3. POST processing 
surfc(X,Y,TT) ;
colormap jet