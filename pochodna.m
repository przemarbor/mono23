close all 
clear all 
clc 

x = -3:0.1:3 ; % n elementow

y  = x.^3 ;  % n elementow

dx = diff(x) ;   % n-1 elementow
dy = diff(y) ;   % n-1 elementow

yp = dy ./ dx ;  % n-1 elementow

d2y = diff( dy ) ; % diff(y,2) % n-2 elementy
dx2 = dx.^2 ; 

yp2 = d2y ./ dx2(2:end) ;

d3y = diff( y, 3 ) ; % n-3 elementy
dx3 = dx.^3 ;

yp3 = d3y ./ dx3(3:end) ;

plot(x,y, 'r')
hold on 
plot(x(2:end) , yp, 'g' )
plot(x(3:end) , yp2, 'b' )
plot(x(4:end) , yp3, 'c' )
hold off 